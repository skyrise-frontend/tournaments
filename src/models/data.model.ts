export interface IData {
  tournamentStart: string;
  tournamentEnd: string;
  teams: number;
  stadiums: number;
  lastWinner: string;
}
