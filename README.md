# tournament-app

## About the demo app

This demo app is made with Vue CLI (version 4.5.9) with VueJS 2.
Used libraries:

* Typescript
* SCSS
* Babel

## Project setup

```text
npm install
```

### Compiles and hot-reloads for development

```text
npm run serve
```

### Compiles and minifies for production

```text
npm run build
```

### Lints and fixes files

```text
npm run lint
```
